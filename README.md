# Evernote Random

View a random note from your Evernote database as often as you'd like (Mac).

## Getting Started
1. View the latest tag [from this list](https://gitlab.com/fortelabs/evernote-random/tags).
2. Download and extract `RandomNote.zip`.
3. Open `RandomNote` to open a random note from your Evernote database. You can drag `RandomNote` to the Dock or your Applications for easy access.

Keep in mind that this project assumes you would like to pull notes from stacks named `1 Projects`, `2 Areas`, and `3 Resources`, as described as part of the PARA implementation in [Building a Second Brain](http://www.buildingasecondbrain.com/). See "Customization" below if you have a different setup.

## Customization

1. Open `RandomEvernote.applescript` in the ScriptEditor (Mac).
1. Edit line 4 to choose the Evernote stacks to include.
1. Run the script to open a random note from those stacks.

You may also consider [exporting the script to an Application for ease-of-use](https://apple.stackexchange.com/questions/8299/how-do-i-make-an-applescript-file-into-a-mac-app).